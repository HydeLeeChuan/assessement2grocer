(function (){
    angular
        .module("martApp")
        .controller("browseCtrl",browseCtrl);

    browseCtrl.$inject = ["$http","$q"];

    function browseCtrl($http, $q){
        var vm = this;

        vm.productName ='';

        vm.result = [];
        vm.search = search;
        vm.delete = _delete;


        init();

        function init(){
            var defer = $q.defer();
            $http
                .get("/api/items")
                .then(function(res){
                    console.log("initialised");
                    vm.items = res.data;
                    defer.resolve(res.data);
                })
                .catch(function(err){
                    console.log(err);
                });
            return (defer.promise);

        }


        function search(productName){

            var defer = $q.defer();
            $http
                .get("/api/items",{
                    params:{
                        'name': vm.productName
                    }
                })
                .then(function(res){
                    console.log("found");
                    vm.items = res.data;
                    defer.resolve(res.data);
                })
                .catch(function(err){
                    console.log(err);
                });
            return (defer.promise);

        }

        function _delete(item) {
            var defer = $q.defer();
            console.log("-- show.controller.js > deleteProduct()");
            $http
                .delete("/api/items/" + item)
                .then(function (response) {
                    console.log("-- show.controller.js > deleteProduct() > res obj after delete req: \n" + response);
                })
                .catch((function (error) {

                }));
            return (defer.promise);
        }


    }

})();
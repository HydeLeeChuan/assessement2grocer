(function(){
    angular
        .module("martApp")
        .config(martAppConfig);

    martAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function martAppConfig($stateProvider, $urlRouterProvider){

        $stateProvider
            .state('A',{
                url:'/A',
                templateUrl : './browse/browse.html',
                controller: 'browseCtrl',
                controllerAs: 'ctrl'
            })
            .state('B',{
                url : '/B',
                templateUrl :'./add/add.html',
                controller : 'addCtrl',
                controllerAs : 'ctrl'
            });
        $urlRouterProvider.otherwise("/A");
    }

})();
(function () {
    angular
        .module("martApp")
        .controller("addCtrl", addCtrl);

    addCtrl.$inject = ["$http"];

    function addCtrl($http) {
        var vm = this;

        vm.item = {};
        vm.status = {
            message: "",
            success: ""
        };

        // Exposed functions
        vm.register = register;

        // Function definition
        function register() {

            $http.post("/api/items", vm.item)
                .then(function (res) {
                    console.info("Success! Response returned: " + JSON.stringify(res.data));
                    vm.status.message = "The product is added to the database.";
                    vm.status.success = "ok";
                    console.info("status: " + JSON.stringify(vm.status));
                })
                .catch(function (err) {
                    console.info("Error: " + err);
                    vm.status.message = "Failed to add the product to the database.";
                    vm.status.success = "error";
                });
        }
    }

})();







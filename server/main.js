var express = require("express");
var app= express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(express.static(__dirname + "/../client"));

var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'WBHyde92';
var conn = new Sequelize('items',MYSQL_USERNAME, MYSQL_PASSWORD,[]);

var Item = require('./models/item')(conn, Sequelize);

conn.sync().then(function(){
    console.log("Database in sync now")
});

//Associations

app.get ("/api/items/:upc12", function (req, res){
    console.log("-- GET /api/departments/:upc12");
    var upc12 = req.query.productCode;

    var where={};

    if(req.params.upc12){
        where.upc12 = req.params.upc12
    }

    // if(req.params.name){
    //     where.name=
    // }
    Item
        .findOne({
            where: where,
            attributes: ['upc12','brand','name']
        })
        .then(function(items){
            res.json(items);
            console.log("GET /api/items");
        })
        .catch(function(err){
            res.status(500);
            console.log(err);
        });

});



app.get ("/api/items", function (req, res){

    var where = {};

    if (req.query.name) {
        where.name = {
            $like: "%" + req.query.name + "%"
        }
    }

    console.log("--GET /api/items");
    Item
        .findAll({
            where: where,
            limit: 20,
            attributes: ['upc12','brand','name']
        })
        .then(function(items){
            res.json(items);
            console.log("GET /api/items");
        })
        .catch(function(err){
            console.log(err);
        });

});

app.post("/api/items", function (req, res) {
    console.log("Query", req.query);
    console.log("Body", req.body);

    Item
        .create({
            upc12: req.body.serial,
            brand: req.body.brand,
            name: req.body.name
        })
        .then(function (item) {
            res
                .status(200)
                .json(item);
        })
        .catch(function (err) {
            console.log("error: " + err);
            res
                .status(500)
                .json({error: true});
        })


});

app.delete("/api/items/:upc12", function (req, res) {
    console.log("-- DELETE /api/items/:upc12");
    console.log("-- DELETE /api/items/:upc12 request params: " + JSON.stringify(req.params));
    var where = {};
    where.upc12 = req.params.upc12;
    console.log("where " + JSON.stringify(where));

    // The dept_manager table's primary key is a composite of dept_no and emp_no
    // We will use these to find our manager. It is important to include dept_no because an employee maybe a
    // manager of 2 or more departments. Even if business logic doesn't support this, always search
    // a table and delete rows of a table based on the defined primary keys
    Item
        .destroy({
            where: where
        })
        .then(function (result) {
            console.log("-- DELETE /api/items/:upc12" + JSON.stringify(result));
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});

        })
        .catch(function (err) {
            console.log("-- DELETE /api/items/:upc12" + JSON.stringify(err));
        });
});





app.listen(3000, function(){
    console.log("webserver started on port 3000");
});
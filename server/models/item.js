/**
 * Created by LeeChuan on 8/11/16.
 */
module.exports= function(conn,Sequelize){
    var Item= conn.define('items', {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,

        },

        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull: false,
            primaryKey: true

        },
        brand: {
            type: Sequelize.STRING,
            allowNull: false
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false
        }
    },{

        tableName:'grocery_list',
        timestamps: false
    });
    return Item;
};